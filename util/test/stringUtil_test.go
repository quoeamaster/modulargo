package test

import (
	"testing"

	"gitlab.com/quoeamaster/modulargo/util"
)

func TestIsEmptyString(t *testing.T) {
	tests := []struct {
		text   string
		result bool
	}{
		{"  ", true},
		{"", true},
		{"something", false},
		{"  some thing else ", false},
	}

	for i, test := range tests {
		if isEmpty := util.IsStringEmpty(test.text); isEmpty != test.result {
			t.Errorf("{%v} expect [%v], but ... supplied text [%v]", i, test.result, test.text)
		}
	}
}
