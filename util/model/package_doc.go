// Package model provides model level supports.
// Remember that in golang, there is no "inheritance" but only polymorphism;
// hence "inheritance" is implemented in the way of composition.
// This package provides a couple of models that would be handy for building bigger models with common functionalities.
package model
