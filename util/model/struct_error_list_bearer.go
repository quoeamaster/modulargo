package model

// ErrorListBearer - a struct with an error List.
type ErrorListBearer struct {
	// The list of errors encountered.
	errorList []error
}

// NewErrorListBearer - create a new Error List bearer model.
// Usually this model would be incorporated to form bigger models.
func NewErrorListBearer() (instance *ErrorListBearer) {
	instance = new(ErrorListBearer)
	instance.errorList = make([]error, 0)
	return
}

// HasError - checks whether any error occured.
func (e *ErrorListBearer) HasError() (has bool) {
	has = len(e.errorList) > 0
	return
}

// GetErrors - return the error(s) occured.
func (e *ErrorListBearer) GetErrors() []error { return e.errorList }

// Reset - reset the error list back to size of 0.
func (e *ErrorListBearer) Reset() *ErrorListBearer {
	e.errorList = make([]error, 0)
	return e
}

// AppendError - appends the given error value to the end of the error list.
func (e *ErrorListBearer) AppendError(err error) (instance *ErrorListBearer) {
	if err != nil {
		e.errorList = append(e.errorList, err)
	}
	instance = e
	return
}
