package util

import (
	"crypto/md5"
	"encoding/hex"
	"regexp"
	"strings"
)

// PatternEMailRegexp - the regexp pattern for EMail.
var PatternEMailRegexp *regexp.Regexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

// IsStringEmpty - detects whether the given string is NULL or empty.
// "isEmpty" returns a bool value indicates whether the given string is null/empty.
// Note that both NULL and empty string returns to "true".
func IsStringEmpty(value string) (isEmpty bool) {
	isEmpty = false

	// Empty string checks
	if strings.Compare(strings.Trim(value, " "), "") == 0 {
		isEmpty = true
	}
	return
}

// HashStringByMD5 - creates a MD5 hashed value. Returns the MD5 hashed value as well as an error object.
func HashStringByMD5(value string) (hash string, err error) {
	_md5 := md5.New()
	_, err = _md5.Write([]byte(value))
	if err != nil {
		return
	}

	hash = hex.EncodeToString(_md5.Sum(nil))
	return
}

// IsValidEMail - validates if the given value is an EMail address.
func IsValidEMail(value string) (valid bool) {
	valid = true

	// Validation. Null check.
	valid = !IsStringEmpty(value)
	if !valid {
		return
	}
	// Validation. Length check.
	if len(value) < 3 || len(value) > 254 {
		valid = false
		return
	}
	// Validation. Pattern check.
	if !PatternEMailRegexp.MatchString(value) {
		valid = false
		return
	}
	return
}
