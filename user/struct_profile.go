package user

import (
	"errors"
	"fmt"

	"gitlab.com/quoeamaster/modulargo/util"
	"gitlab.com/quoeamaster/modulargo/util/model"
)

// ProfileModelVer - the current version of the Profile model.
const ProfileModelVer = 1.0

// The ProfileV1_0 struct encapsulates the information required to setup a "User" entity.
// The "version" indicates the member attributes available for a particular version of the struct. Usually would invoke the
// type "Profile" instead of pinpointing the version specific Struct.
//
// See Also
//
// The [Profile] type
type ProfileV1_0 struct {
	// The data storage generated ID for this user / profile. Storage specific, could be a running number or UUID.
	id string

	// The name of the user / profile.
	name string
	// The hashed password by MD5 (optional if other authentication schemes were chosen).
	// This is a write-only attribute; no getter is available for security reasons.
	passwordMd5 string
	// The email address for communication and authentication purpose.
	email string
	// The language code preferred (e.g. en-us).
	langCode string
	// The currency code preferred for receiving donation / fees (e.g. USD).
	currencyCode string
	// A description about this user / profile. Important for being searchable.
	description string

	// Whether this user / profile is searchable.
	// Public is recommended to contact potential non-fans on the system.
	// Private simply means only fans added by profile owner could enjoy the owner's works.
	public bool

	// The Shipping address related to the user / profile.
	shippingAddr ShippingAddr

	// The avatar (profile pic) stored in base64 format
	avatarBase64 string

	// A list of error occur during the build of the model.
	ErrorList model.ErrorListBearer
}

// Profile is the type referring to the latest ProfileV{major}_{minor} version of a user / profile.
type Profile ProfileV1_0

// NewProfile - creates a new Profile model.
func NewProfile() (instance *Profile) {
	instance = new(Profile)
	instance.ErrorList = *model.NewErrorListBearer()
	return
}

// getVersionNum - returns the version of the underlying Profile struct.
func (p *Profile) getVersionNum() (ver float32) {
	ver = ProfileModelVer
	return
}

// TODO: remove this line after done ***  get / setters ***

// ID - set the id of the profile.
func (p *Profile) ID(value string) (instance *Profile) {
	// Validation. Null check
	if util.IsStringEmpty(value) {
		p.ErrorList.AppendError(errors.New("[id] Invalid, reason: empty value"))
	} else {
		// Format validation? Nope, since ID is data storage specific. Hence not necessary.
		p.id = value
	}
	instance = p
	return
}

// GetID - returns the id value.
func (p *Profile) GetID() string { return p.id }

// Name - set the name of the profile.
func (p *Profile) Name(value string) (instance *Profile) {
	// Validation. Null check
	if util.IsStringEmpty(value) {
		p.ErrorList.AppendError(errors.New("[name] Invalid, reason: empty value"))
	} else {
		p.name = value
	}
	instance = p
	return
}

// GetName - returns the name value.
func (p *Profile) GetName() string { return p.name }

// Password - set the password value.
func (p *Profile) Password(value string) (instance *Profile) {
	// Validation. Null check
	if util.IsStringEmpty(value) {
		p.ErrorList.AppendError(errors.New("[password] Invalid, reason: empty value"))
	} else {
		// Hashing.
		_md5, err := util.HashStringByMD5(value)
		if err != nil {
			p.ErrorList.AppendError(fmt.Errorf("[password] Invalid, reason: %v", err))
		} else {
			p.passwordMd5 = _md5
		}
	}
	instance = p
	return
}

// EMail - set the email value.
func (p *Profile) EMail(value string) (instance *Profile) {
	// Validation. Null check.
	if util.IsStringEmpty(value) {
		p.ErrorList.AppendError(errors.New("[email] Invalid, reason: empty value"))
	} else if !util.IsValidEMail(value) {
		// Validation. Email format check.
		p.ErrorList.AppendError(fmt.Errorf("[email] Invalid, reason: wrong EMail format -> %v", value))
	} else {
		p.email = value
	}
	instance = p
	return
}

// GetEMail - returns the email value.
func (p *Profile) GetEMail() string { return p.email }

// LangCode - set the lang code value.
func (p *Profile) LangCode(value string) (instance *Profile) {
	// Validation. Null Check.
	if util.IsStringEmpty(value) {
		p.ErrorList.AppendError(errors.New("[langcode] Invalid, reason: empty value"))
	} else if !util.IsLangCodeValid(value) {
		// Validation. Check langcode validity.
		p.ErrorList.AppendError(fmt.Errorf("[langcode] Invalid, reason: not supported langCode -> %v", value))
	} else {
		p.langCode = value
	}
	instance = p
	return
}

// GetLangCode - returns the lang code value.
func (p *Profile) GetLangCode() string { return p.langCode }
