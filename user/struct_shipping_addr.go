package user

import (
	"errors"
	"fmt"

	"gitlab.com/quoeamaster/modulargo/util"
	"gitlab.com/quoeamaster/modulargo/util/model"
)

// ShippingAddr struct represents a shipping address model.
// Since for most cases the shipping address model would be stable even if the Profile model is everchanging;
// hence it is a good idea to separate it out.
type ShippingAddr struct {
	// The country code.
	countryCode string
	// Address line 1.
	line1 string
	// Address line 2 (optional).
	line2 string
	// Address line 3 (optional).
	line3 string
	// City name.
	city string
	// State or District.
	state string
	// Zipcode or postal code (optional).
	zipCode string

	// Errors involved in creating the shipping address model. This attributer is public access.
	ErrorList model.ErrorListBearer
}

// NewShippingAddr - creates and returns an instance of ShippingAddr.
// Note that all attributes are not set at this moment.
func NewShippingAddr() (instance *ShippingAddr) {
	instance = new(ShippingAddr)
	instance.ErrorList = *model.NewErrorListBearer()
	return
}

// Reset - reset the model to default values.
func (s *ShippingAddr) Reset() (instance *ShippingAddr) {
	s.countryCode = ""
	s.line1 = ""
	s.line2 = ""
	s.line3 = ""
	s.city = ""
	s.state = ""
	s.zipCode = ""
	s.ErrorList = *s.ErrorList.Reset()

	instance = s
	return s
}

// CountryCode - set the countryCode value for the shipping address.
// Validations: a) null check, b) country code validity check
func (s *ShippingAddr) CountryCode(value string) (instance *ShippingAddr) {
	// Validation. Null check.
	_isEmpty := util.IsStringEmpty(value)
	if _isEmpty {
		s.countryCode = ""
		s.ErrorList.AppendError(errors.New("[countryCode] Invalid, reason: empty value"))
	} else {
		// Validation. is countryCode valid
		_isValid := util.IsCountryCodeValid(value)
		if !_isValid {
			s.ErrorList.AppendError(fmt.Errorf("[countryCode] Invalid, reason: non valid value -> %v", value))
		} else {
			s.countryCode = value
		}
	}
	instance = s
	return
}

// GetCountry - returns the country code and country name associated.
func (s *ShippingAddr) GetCountry() (code string, name string) {
	code = s.countryCode
	name = util.LookupCountryCodes[s.countryCode]
	return
}

// Line1 - set the shipping address line 1. This field is mandatory in a shipping address.
// Note that for line2 and line3, they are both optional.
func (s *ShippingAddr) Line1(value string) (instance *ShippingAddr) {
	// Validation. Null check.
	_isEmpty := util.IsStringEmpty(value)
	if _isEmpty {
		s.ErrorList.AppendError(errors.New("[line1] Invalid, reason: empty value"))
	} else {
		s.line1 = value
	}
	instance = s
	return
}

// GetLine1 - return the shipping address line1.
func (s *ShippingAddr) GetLine1() string { return s.line1 }

// Line2 - sets the shipping address line 2.
// No validation is applied as this field is optional.
func (s *ShippingAddr) Line2(value string) *ShippingAddr {
	s.line2 = value
	return s
}

// GetLine2 - return the shipping address line2.
func (s *ShippingAddr) GetLine2() string { return s.line2 }

// Line3 - sets the shipping address line 3.
// No validation is applied as this field is optional.
func (s *ShippingAddr) Line3(value string) *ShippingAddr {
	s.line3 = value
	return s
}

// GetLine3 - return the shipping address line3.
func (s *ShippingAddr) GetLine3() string { return s.line3 }

// City - sets the city name. Value is optional and hence no validations.
func (s *ShippingAddr) City(value string) *ShippingAddr {
	s.city = value
	return s
}

// GetCity - returns the city value.
func (s *ShippingAddr) GetCity() string { return s.city }

// State - sets the state name. Value is optional and hence no validations.
func (s *ShippingAddr) State(value string) *ShippingAddr {
	s.city = value
	return s
}

// GetState - returns the state value.
func (s *ShippingAddr) GetState() string { return s.state }

// ZipCode - sets the zip code. Value is optional and hence no validations.
func (s *ShippingAddr) ZipCode(value string) *ShippingAddr {
	s.zipCode = value
	return s
}

// GetZipCode - returns the zipCode value.
func (s *ShippingAddr) GetZipCode() string { return s.zipCode }
